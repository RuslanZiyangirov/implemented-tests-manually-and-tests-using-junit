package itis.quiz.spaceships.test_with_JUnit;

import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CommandCenterTest {

    static CommandCenter commandCenter;
    ArrayList<Spaceship> spaceships;

    @BeforeAll
    public static void setUp(){
        commandCenter = new CommandCenter();
    }

    @BeforeEach
    public void beforeTest(){
        System.out.println("Выполняется тест...");
        spaceships = new ArrayList<>();
    }


    @AfterAll
    public static void afterTest() {
        System.out.println("Тест выполнился.");
    }

    @Test
    @DisplayName("Возврат корабля с наибольшей огневой мощью.")
    void getMostPowerfulShip_HaveShip() {

        spaceships.add(new Spaceship("Topol", 1000, 5678, 3567));
        spaceships.add(new Spaceship("Belka", 1260, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 1578, 4778, 2967));
        spaceships.add(new Spaceship("Bereza", 970, 2868, 1667));
        spaceships.add(new Spaceship("Pushka", 670, 3568, 2357));

        Assertions.assertEquals(1578,commandCenter.getMostPowerfulShip(spaceships).getFirePower());
    }

    @Test
    @DisplayName("Возврат Null., у метода огневой мощи.")
    void getMostPowerfulShipTest_IsNull(){
        spaceships.add(new Spaceship("Topol", 0, 5678, 3567));
        spaceships.add(new Spaceship("Belka", 0, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 0, 4778, 2967));
        spaceships.add(new Spaceship("Bereza", 0, 2868, 1667));
        spaceships.add(new Spaceship("Pushka", 0, 3568, 2357));

        assertNull(commandCenter.getMostPowerfulShip(spaceships));
    }

    @Test
    @DisplayName("Возврат false,по поиску макс.знач")
    void getMostPowerfulShipTest_IsBad(){

        spaceships.add(new Spaceship("Topol", 564, 5678, 3567));
        spaceships.add(new Spaceship("Belka", 765, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 234, 4778, 2967));
        spaceships.add(new Spaceship("Bereza", 567, 2868, 1667));
        spaceships.add(new Spaceship("Pushka", 345, 3568, 2357));

        Assertions.assertEquals(false,commandCenter.getMostPowerfulShip(spaceships).getFirePower() == 234);

    }

    @Test
    @DisplayName("Возвращает имя Topol")
    void getShipByNameTest() {

        spaceships.add(new Spaceship("Topol", 564, 5678, 3567));
        spaceships.add(new Spaceship("Belka", 765, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 234, 4778, 2967));
        spaceships.add(new Spaceship("Bereza", 567, 2868, 1667));
        spaceships.add(new Spaceship("Pushka", 345, 3568, 2357));


        Assertions.assertEquals("Topol",commandCenter.getShipByName(spaceships, "Topol").getName());

    }

    @Test
    @DisplayName("Возврат Null. при вызове имени у коробля ")
    void getShipByNameTestIsNull(){

        spaceships.add(new Spaceship("", 564, 5678, 3567));
        spaceships.add(new Spaceship("", 765, 658, 3467));
        spaceships.add(new Spaceship("", 234, 4778, 2967));
        spaceships.add(new Spaceship("", 567, 2868, 1667));
        spaceships.add(new Spaceship("", 345, 3568, 2357));

        assertNull(commandCenter.getShipByName(spaceships, "Bereza"));
    }

    @Test
    @DisplayName("Возврат true, что существую корабли с отсеком > 2500 ")
    void getAllShipsWithEnoughCargoSpaceTest() {

        boolean test1 = false;
        spaceships.add(new Spaceship("Topol", 564, 5678, 3567));
        spaceships.add(new Spaceship("Belka", 765, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 234, 4778, 2967));
        spaceships.add(new Spaceship("Bereza", 567, 2868, 1667));
        spaceships.add(new Spaceship("Pushka", 345, 3568, 2357));

        commandCenter.getAllShipsWithEnoughCargoSpace(spaceships, 2500);

            for (Spaceship spaceship : spaceships) {
                if (spaceship.getCargoSpace() >= 2500) {
                    test1 = true;
                    break;
                }

            }

        assertTrue(test1);
    }

    @Test
    @DisplayName("Вернет true, что не существует корабля с отсеком > 2500")
    public void getAllShipsWithEnoughCargoSpaceTestIsEmpty(){

        spaceships.add(new Spaceship("Topol", 564, 0, 3567));
        spaceships.add(new Spaceship("Belka", 765, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 234, 477, 2967));
        spaceships.add(new Spaceship("Bereza", 567, 28, 1667));
        spaceships.add(new Spaceship("Pushka", 345, 356, 2357));


        assertTrue(commandCenter.getAllShipsWithEnoughCargoSpace(spaceships, 2500).isEmpty());

    }

    @Test
    @DisplayName("Вернет true, если существует хотя бы 1 мирный корабль.")
    void getAllCivilianShips() {

        boolean test2 = false;
        spaceships.add(new Spaceship("Topol", 564, 0, 3567));
        spaceships.add(new Spaceship("Belka", 765, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 0, 477, 2967));
        spaceships.add(new Spaceship("Bereza", 567, 28, 1667));
        spaceships.add(new Spaceship("Pushka", 345, 356, 2357));

        for (Spaceship spaceship : spaceships) {
            if (spaceship.getFirePower() == 0) {
                test2 = true;
            }
        }

        assertTrue(test2);
    }

    @Test
    @DisplayName("Вернет true, если мирных кораблей не существует.")
    public void getAllCivilianShipsTestIsEmpty(){
        spaceships.add(new Spaceship("Topol", 564, 0, 3567));
        spaceships.add(new Spaceship("Belka", 765, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 234, 477, 2967));
        spaceships.add(new Spaceship("Bereza", 567, 28, 1667));
        spaceships.add(new Spaceship("Pushka", 345, 356, 2357));

        assertTrue(commandCenter.getAllCivilianShips(spaceships).isEmpty());
    }
}