package itis.quiz.spaceships;


import java.util.ArrayList;
import java.util.Scanner;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{

    Object object = new Object();

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int max = 0;
        for (Spaceship ship : ships) {
            if (max < ship.getFirePower()) {
                max = ship.getFirePower();
                object = ship;
            }
        }

        if (max == 0) {
            return null;
        }
        return (Spaceship) object;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships,String name) {

        for (Spaceship ship : ships) {
            if (name.equals(ship.getName())) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> spaceships,Integer cargoSize) {
        ArrayList<Spaceship> cargoSpace = new ArrayList<>();

        for (Spaceship spaceship : spaceships) {
            if (spaceship.getCargoSpace() >= cargoSize) {
                cargoSpace.add(spaceship);
            }

        }
        return cargoSpace;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {

        ArrayList<Spaceship> spaceshipsPower = new ArrayList<>();

        for (Spaceship spaceship: ships){
            if (spaceship.getFirePower() == 0){
                spaceshipsPower.add(spaceship);
            }
        }
        return spaceshipsPower;
    }
}
