package itis.quiz.spaceships;

import java.util.ArrayList;
import java.util.Objects;



public class CommandCenterUnitTest {

    public  CommandCenter commandCenter = new CommandCenter();

    public static void main(String[] args) {
        CommandCenterUnitTest commandCenterUnitTest = new CommandCenterUnitTest();
        System.out.println("getMostPowerfulShipTest_HaveShip -" + (commandCenterUnitTest.getMostPowerfulShipTest_HaveShip()));
        System.out.println("getMostPowerfulShipTest_IsNull -" + commandCenterUnitTest.getMostPowerfulShipTest_IsNull());
        System.out.println("getMostPowerfulShipTest_IsBad -" + commandCenterUnitTest.getMostPowerfulShipTest_IsBad());
        System.out.println("getShipByNameTest1 -" + commandCenterUnitTest.getShipByNameTest());
        System.out.println("getShipByNameTestIsNull -" + commandCenterUnitTest.getShipByNameTest());
        System.out.println("getAllShipsWithEnoughCargoSpaceTest -" + commandCenterUnitTest.getAllShipsWithEnoughCargoSpaceTest());
        System.out.println("getAllShipsWithEnoughCargoSpaceTestIsEmpty -" + commandCenterUnitTest.getAllShipsWithEnoughCargoSpaceTestIsEmpty());
        System.out.println("getAllCivilianShipsTestPeacefulShips -" +commandCenterUnitTest.getAllCivilianShipsTestPeacefulShips());
        System.out.println("getAllCivilianShipsTestIsEmpty -" +commandCenterUnitTest.getAllCivilianShipsTestIsEmpty());
        double result = 0.0;
        if (commandCenterUnitTest.getMostPowerfulShipTest_HaveShip()){
            result = result + 0.4;
        }
        if (commandCenterUnitTest.getMostPowerfulShipTest_IsNull()){
            result = result + 0.4;
        }
        if (commandCenterUnitTest.getMostPowerfulShipTest_IsBad()){
            result = result + 0.4;
        }
        if(commandCenterUnitTest.getShipByNameTest()){
            result = result + 0.4;
        }
        if(commandCenterUnitTest.getShipByNameTestIsNull()){
            result = result + 0.4;
        }
        if(commandCenterUnitTest.getAllShipsWithEnoughCargoSpaceTest()){
            result = result + 0.4;
        }
        if(commandCenterUnitTest.getAllShipsWithEnoughCargoSpaceTestIsEmpty()){
            result = result + 0.4;
        }
        if(commandCenterUnitTest.getAllCivilianShipsTestPeacefulShips()){
            result = result + 0.4;
        }
        if(commandCenterUnitTest.getAllCivilianShipsTestIsEmpty()){
            result = result + 0.4;
        }
        System.out.println("Количество баллов: " + result);

    }

    public boolean getMostPowerfulShipTest_HaveShip() {
        ArrayList<Spaceship> spaceships = new ArrayList<>();

        spaceships.add(new Spaceship("Topol", 1000, 5678, 3567));
        spaceships.add(new Spaceship("Belka", 1260, 658, 3467));
        spaceships.add(new Spaceship("Strelka", 1578, 4778, 2967));
        spaceships.add(new Spaceship("Bereza", 970, 2868, 1667));
        spaceships.add(new Spaceship("Pushka", 670, 3568, 2357));

        return commandCenter.getMostPowerfulShip(spaceships).getFirePower() == 1578;
    }

    public boolean getMostPowerfulShipTest_IsNull() {
        ArrayList<Spaceship> ships = new ArrayList<>();

        ships.add(new Spaceship("Topol", 0, 5678, 3567));
        ships.add(new Spaceship("Belka", 0, 658, 3467));
        ships.add(new Spaceship("Strelka", 0, 4778, 2967));
        ships.add(new Spaceship("Bereza", 0, 2868, 1667));
        ships.add(new Spaceship("Pushka", 0, 3568, 2357));

        return Objects.isNull(commandCenter.getMostPowerfulShip(ships));
    }

    public boolean getMostPowerfulShipTest_IsBad() {
        ArrayList<Spaceship> shipss = new ArrayList<>();

        shipss.add(new Spaceship("Topol", 564, 5678, 3567));
        shipss.add(new Spaceship("Belka", 765, 658, 3467));
        shipss.add(new Spaceship("Strelka", 234, 4778, 2967));
        shipss.add(new Spaceship("Bereza", 567, 2868, 1667));
        shipss.add(new Spaceship("Pushka", 345, 3568, 2357));

        return commandCenter.getMostPowerfulShip(shipss).getFirePower() == 234;
    }

    public boolean getShipByNameTest() {
        ArrayList<Spaceship> shipsss = new ArrayList<>();

        shipsss.add(new Spaceship("Topol", 564, 5678, 3567));
        shipsss.add(new Spaceship("Belka", 765, 658, 3467));
        shipsss.add(new Spaceship("Strelka", 234, 4778, 2967));
        shipsss.add(new Spaceship("Bereza", 567, 2868, 1667));
        shipsss.add(new Spaceship("Pushka", 345, 3568, 2357));


        return commandCenter.getShipByName(shipsss, "Topol").getName().equals("Topol");
    }

    public boolean getShipByNameTestIsNull() {
        ArrayList<Spaceship> shipsss = new ArrayList<>();

        shipsss.add(new Spaceship("", 564, 5678, 3567));
        shipsss.add(new Spaceship("", 765, 658, 3467));
        shipsss.add(new Spaceship("", 234, 4778, 2967));
        shipsss.add(new Spaceship("", 567, 2868, 1667));
        shipsss.add(new Spaceship("", 345, 3568, 2357));

        return Objects.isNull(commandCenter.getShipByName(shipsss, "Topol"));
    }

    public boolean getAllShipsWithEnoughCargoSpaceTest() {
        ArrayList<Spaceship> spaceshipArrayList = new ArrayList<>();
        boolean test1 = false;
        spaceshipArrayList.add(new Spaceship("Topol", 564, 5678, 3567));
        spaceshipArrayList.add(new Spaceship("Belka", 765, 658, 3467));
        spaceshipArrayList.add(new Spaceship("Strelka", 234, 4778, 2967));
        spaceshipArrayList.add(new Spaceship("Bereza", 567, 2868, 1667));
        spaceshipArrayList.add(new Spaceship("Pushka", 345, 3568, 2357));

        commandCenter.getAllShipsWithEnoughCargoSpace(spaceshipArrayList, 2500);

        for (Spaceship spaceship : spaceshipArrayList) {
            if (spaceship.getCargoSpace() >= 2500) {
                test1 = true;
                break;
            }

        }

        return test1;
    }

    public boolean getAllShipsWithEnoughCargoSpaceTestIsEmpty() {
        ArrayList<Spaceship> spaceshipArrayList = new ArrayList<>();
        spaceshipArrayList.add(new Spaceship("Topol", 564, 0, 3567));
        spaceshipArrayList.add(new Spaceship("Belka", 765, 658, 3467));
        spaceshipArrayList.add(new Spaceship("Strelka", 234, 477, 2967));
        spaceshipArrayList.add(new Spaceship("Bereza", 567, 28, 1667));
        spaceshipArrayList.add(new Spaceship("Pushka", 345, 356, 2357));


        return commandCenter.getAllShipsWithEnoughCargoSpace(spaceshipArrayList, 2500).isEmpty();
    }

    public boolean getAllCivilianShipsTestPeacefulShips() {
        ArrayList<Spaceship> arrayList = new ArrayList<>();
        boolean test2 = false;
        arrayList.add(new Spaceship("Topol", 564, 0, 3567));
        arrayList.add(new Spaceship("Belka", 765, 658, 3467));
        arrayList.add(new Spaceship("Strelka", 0, 477, 2967));
        arrayList.add(new Spaceship("Bereza", 567, 28, 1667));
        arrayList.add(new Spaceship("Pushka", 345, 356, 2357));

        for (Spaceship spaceship : arrayList) {
            if (spaceship.getFirePower() == 0) {
                test2 = true;
            }
        }

        return test2;
    }

    public boolean getAllCivilianShipsTestIsEmpty()

    {
        ArrayList<Spaceship> arrayList = new ArrayList<>();
        arrayList.add(new Spaceship("Topol", 564, 0, 3567));
        arrayList.add(new Spaceship("Belka", 765, 658, 3467));
        arrayList.add(new Spaceship("Strelka", 234, 477, 2967));
        arrayList.add(new Spaceship("Bereza", 567, 28, 1667));
        arrayList.add(new Spaceship("Pushka", 345, 356, 2357));

        return commandCenter.getAllCivilianShips(arrayList).isEmpty();
    }







}
